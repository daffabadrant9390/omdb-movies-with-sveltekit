import { writable } from 'svelte/store';

export const moviesData = writable([]);

export const fetchMoviesData = async (title) => {
	const url = `http://www.omdbapi.com/?apikey=7c36f70a&s=${title}`;
	const res = await fetch(url);
	const movies = await res.json();
	console.log(url);
	console.log(title);

	moviesData.set(movies.Search);
};

fetchMoviesData('avengers');
